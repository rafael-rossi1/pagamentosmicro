package br.com.itau.pagamentosmicro.services;

import br.com.itau.pagamentosmicro.client.CreditCardClient;
import br.com.itau.pagamentosmicro.models.Payment;
import br.com.itau.pagamentosmicro.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    CreditCardClient creditCardClient;

    public Payment create(Payment payment) {
        payment.setNumber(creditCardClient.getCardNumber(payment.getNumber()));
        return paymentRepository.save(payment);
    }

    public Iterable<Payment> findAllPayments() {
        return paymentRepository.findAll();
    }

}
