package br.com.itau.pagamentosmicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PagamentosmicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagamentosmicroApplication.class, args);
	}

}
