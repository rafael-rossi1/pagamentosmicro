package br.com.itau.pagamentosmicro.repositories;

import br.com.itau.pagamentosmicro.models.Payment;
import org.springframework.data.repository.CrudRepository;

public interface PaymentRepository extends CrudRepository<Payment, Long> {
}
