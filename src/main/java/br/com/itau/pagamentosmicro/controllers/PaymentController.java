package br.com.itau.pagamentosmicro.controllers;


import br.com.itau.pagamentosmicro.models.Payment;
import br.com.itau.pagamentosmicro.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pagamentos")

public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Payment createPayment(@RequestBody @Valid Payment payment) {
        return paymentService.create(payment);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Payment> findAllPayments() {
        Iterable<Payment> payments = paymentService.findAllPayments();
        return payments;
    }
}
