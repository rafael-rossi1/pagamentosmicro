package br.com.itau.pagamentosmicro.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CAR")
public interface CreditCardClient {

        @GetMapping("/{cartao}")
        String getCardNumber(@PathVariable String number);

    }
